<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title>Team creator, reviewer</title>
		<link rel = "stylesheet" href = "/css/style.css" type = "text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	</head>
	<body>
		<header>
			<ul class = "menu">
				<li><a href = "/">Home</a></li>
				<li><a href = "/team/">Team stats</a></li>
				<li><a href = "/matches/">Matches</a></li>
			</ul>
		</header>
		<div id="wrapper">
			<div class="main_block">
				<form action="/scripts/team_info/add_team.php" method="post" enctype="multipart/form-data">
					<div class = "main_block_top">
						<div class = "main_block_top_left">
							<h1><b>Info about team:</b></h1>
							<input type="text" name="team_tag" placeholder = "Enter team tag here">
						</div>
						<div class = "main_block_top_right">
							<select name="team" id="team">
								<option value="No team selected">No team selected</option>
								<?php
									include_once("../scripts/db/config.php");
									$query = 'SELECT * FROM teams';
									$res = mysql_query($query);
									if(!$res)
									{
										die(mysql_errno() . ": " . mysql_error() . "<br>");
									}
									while($data = mysql_fetch_array($res))
									{
										printf('<option value="%s">%s</option>',$data['team_tag'], $data['team_tag']);
									}
								?>
							</select>
							<script src = "../scripts/team_info/print_team.js"></script>
							<script src = "../scripts/team_stats/print_team_stats.js"></script>
						</div>
					</div>
					<div style="clear:left;"></div>
					<div class="main_block_left">
						<div class="team_logo">
							<input type = "file" name = "team_logo" id = "team_logo">
							<label for="team_logo">
								<div class = "team_logo"> </div>
							</label>
						</div>
					</div>
					<div class="main_block_right">
						<div class="block_player_list">
							<ul style="padding: 10px;">
								<li class="player_list"><input type="text" name="player1"></li>
								<li class="player_list"><input type="text" name="player2"></li>
								<li class="player_list"><input type="text" name="player3"></li>
								<li class="player_list"><input type="text" name="player4"></li>
								<li class="player_list"><input type="text" name="player5"></li>
							</ul>
						</div>
						<input type = "submit" value="Refresh">
					</div>
				</form>
			</div>
			<div class="main_block">
				<div id = "block_team_stats" style = "visibility:hidden">
					<ul class = "team_stats" style = "visibility:hidden">
						<li id = "stats_maps_played">Maps played: 0</li>
						<li id = "stats_wins">Wins: 0</li>
						<li id = "stats_loses">Loses: 0</li>
						<li id = "stats_kd_ratio">Kill/Death ratio: 0</li>
						<li id = "stats_rank">Total rank: 0</li>
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>