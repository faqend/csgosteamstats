<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title>Team creator, reviewer</title>
		<link rel = "stylesheet" href = "/css/style.css" type = "text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	</head>
	<body>
		<header>
			<ul class = "menu">
				<li><a href = "/">Home</a></li>
				<li><a href = "/team/">Team stats</a></li>
				<li><a href = "/matches/">Matches</a></li>
			</ul>
		</header>
		<div id="wrapper">
			<div id="main_block_advices">
				<?php
					include_once($_SERVER['DOCUMENT_ROOT'].'/scripts/matches/classes/match_printer.php');
					$gamesPrinter = new MatchPrinter();
					$gamesPrinter->printMatch();
				?>
			</div>
		</div>
	</body>
</html>