$('select').change( function() {
	var teamTag = this.value;
	if(teamTag == "No team selected")
	{
		$('input[name = "player1').val('');
		$('input[name = "player2').val('');
		$('input[name = "player3').val('');
		$('input[name = "player4').val('');
		$('input[name = "player5').val('');
		$('input[name = "team_tag').val('');
		$('.team_logo').css({
				'background': 'url("/img/team_logo/no logo.png") no-repeat',
				'background-size': 'contain',
				'-webkit-background-size': 'contain',
				'-moz-background-size': 'contain',
				'-o-background-size': 'contain'
			});
	}
	else
	{
		$.ajax({
			type: "POST",
			url: "../scripts/team_info/print_team.php",
			data: { team_tag: teamTag }
		})
		.done(function(data) {
			var team = JSON.parse(data);
			$('input[name = "player1').val(team['players'][1]);
			$('input[name = "player2').val(team['players'][2]);
			$('input[name = "player3').val(team['players'][3]);
			$('input[name = "player4').val(team['players'][4]);
			$('input[name = "player5').val(team['players'][5]);
			$('input[name = "team_tag').val(team['teamTag']);
			var teamLogoSrc = team['teamLogoSrc'];
			$('.team_logo').css({
				'background': 'url("'+teamLogoSrc+'") no-repeat',
				'background-size': 'contain',
				'-webkit-background-size': 'contain',
				'-moz-background-size': 'contain',
				'-o-background-size': 'contain'
			});
		});		
	}
});