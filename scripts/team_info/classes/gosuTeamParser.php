<?php
	class GosuTeamParser
	{
		private $PAGES_LIMIT = 27;	// ** This is limit of pages on "http://www.gosugamers.net/counterstrike/teams?members_from=5&page="
		public $teamPlayers;		// ** Not work yet
		public $teamLogoUrlList;	// ** List of all team logo urls.			$teamLogoUrlList[ team tag ] = logo url
		public $teamUrlList;		// ** List of all team profile urls. 		$teamUrlList[team tag] = team url
		public $teamNameList;		// ** List of all names and tags of teams. 	$teamNameList[ team name ] = team tag
		private $pagesList;

		function __construct()
		{
			$this->parsePagesUrl();
		}

		private function parsePagesUrl()
		{
			$url = 'http://www.gosugamers.net/counterstrike/teams?members_from=5&page=';

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			for($i = 1; $i <= $this->PAGES_LIMIT; $i++)
			{
				$tempUrl = $url.$i;
				$this->pagesList[$i] = file_get_html($tempUrl);
			}
		}

		/*
		*	This Function parse team name and team tag. 
		*	Then save in array $teamNameList[ team name ] = team tag
		*/

		public function parseTeamName()
		{
			//$url = 'http://www.gosugamers.net/counterstrike/teams?members_from=5&page=';

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			for($i = 1; $i <= $this->PAGES_LIMIT; $i++)
			{
				//$tempUrl = $url.$i;

				//echo $tempUrl.'<br>';

				//$html = file_get_html($tempUrl);
				$html = $this->pagesList[$i];
				$html = $html->find("div.team");

				foreach ($html as $div)
				{
					$localText = $div->plaintext;
					$localText = trim($localText);
					//echo $localText.'<br>';

					$teamName = $div->children(1)->children(0)->children(0)->plaintext;

					if($div->children(1)->children(1)->children(0)->children(0)->plaintext == "Owner:")
					{
						$teamTag = $div->children(1)->children(1)->children(1)->children(1)->plaintext;
					}
					else
					{
						$teamTag = $div->children(1)->children(1)->children(0)->children(1)->plaintext;
					}

					/*
					foreach (count_chars($teamName, 1) as $j => $val) {
					   echo "\"" , chr($j) , "\" встречается в строке $val раз(а).\n";
					}
					*/		// I End here! PHP Notice:  Undefined index: Ignite  

					
					$teamName = trim($teamName);
					$teamTag = trim($teamTag);
					//echo 'here team name: '.$teamName.'<br> and team tag: '.$teamTag.'<br>';
					$this->teamNameList[$teamName] = $teamTag;

					
				}
			}
			//var_dump($this->teamNameList);
		}


		/*
		*	This function parse url of all teams on page http://www.gosugamers.net/counterstrike/teams/ 
		*	and save in array $teamUrlList[team tag] = team url
		*/

		public function parseTeamUrl()
		{
			//$url = 'http://www.gosugamers.net/counterstrike/teams?members_from=5&page=';

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			for($i = 1; $i <= $this->PAGES_LIMIT; $i++)
			{
				//$tempUrl = $url.$i;

				//$html = file_get_html($tempUrl);
				$html = $this->pagesList[$i];
				$html = $html->find("div.details div.header a");
				foreach ($html as $div)
				{
					$div->plaintext = trim($div->plaintext);
					$div->href = trim($div->href);
					$this->teamUrlList[$div->plaintext] = 'http://www.gosugamers.net'.$div->href;
					//echo $teamUrlList[$div->plaintext].'<br>';
				}
			}
		}

		/*
		*	This function parse team logo urls.
		*	Save in array $teamLogoUrlList[ team tag ] = logo url
		*/

		public function parseTeamLogoUrl()
		{
			//$url = 'http://www.gosugamers.net/counterstrike/teams?members_from=5&page=';

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			for($i = 1; $i <= $this->PAGES_LIMIT; $i++)
			{
				//$tempUrl = $url.$i;

				//$html = file_get_html($tempUrl);
				$html = $this->pagesList[$i];
				$html = $html->find("div.image img");

				// *** $flag - without this variable parser will take logo url two times. 
				// *** Do not touch this!

				$flag = false;
				foreach ($html as $div)
				{
					if($flag == true)
					{
						$teamTag = $div->parent()->parent()->parent()->children(1)->children(0)->children(0)->plaintext;
						$logoUrl = 'http://www.gosugamers.net'.$div->src;

						$teamTag = trim($teamTag);
						$logoUrl = trim($logoUrl);

						$logoUrl = htmlspecialchars_decode($logoUrl);

						$this->teamLogoUrlList[$teamTag] = $logoUrl;

						$flag = false;

						//echo $teamTag.' logo url: '.$this->teamLogoUrlList[$teamTag].'<br>';
					}
					else
						$flag = true;
				}
			}
			//var_dump($this->teamLogoUrlList);
		}


	}
	// DEBUG
	//$gosuTeamParser = new GosuTeamParser();
	//$gosuTeamParser->parseTeamLogoUrl();
?>