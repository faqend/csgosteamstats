<?php
	class Team
	{
		private $PATH_TO_SAVE = "http://csteamstats.lc/img/team_logo/";
		public $teamLogoSrc;
		public $teamTag;
		public $teamAlias;
		public $players;

		function __construct($teamTag, $teamAlias, $players, $teamLogoSrc)
		{
			$teamTag = trim($teamTag); $teamAlias = trim($teamAlias);
			$teamTag = str_replace("'", "", $teamTag); $teamAlias = str_replace("'", "", $teamAlias);
			$this->teamTag = $teamTag;
			$this->teamAlias = $teamAlias;
			$this->players = $players;
			$this->teamLogoSrc = $teamLogoSrc;
		}

		/*
		*	Function for export team to Data Base
		*/

		public function exportToDB()
		{
			include_once($_SERVER['DOCUMENT_ROOT']."/scripts/db/config.php");
			
			if(empty($this->teamTag) || empty($this->teamAlias) || empty($this->teamLogoSrc))
				die("Export to Data Base error, some variable is empty");

			$query = 'INSERT INTO `csteamstats`.`teams`(id, team_tag, team_alias, team_logo_src, player1, player2, player3, player4, player5) VALUES("NULL", "'.$this->teamTag.'", "'.$this->teamAlias.'", "'.$this->teamLogoSrc.'", "'.$this->players[1].'", "'.$this->players[2].'", "'.$this->players[3].'", "'.$this->players[4].'", "'.$this->players[5].'")';
			
			$res = mysql_query($query);
			
			if(!$res)
				die(mysql_errno() . ": " . mysql_error() . "<br>");
		}


		public function importFromDB()
		{
			include_once($_SERVER['DOCUMENT_ROOT']."/scripts/db/config.php");

			$query = "SELECT * FROM teams WHERE team_tag = '".$this->teamTag."'";
			$res = mysql_query($query);

			if($res == false){
				die(mysql_errno() . ": " . mysql_error() . "<br>");
			}
			else 
			{
				$data = mysql_fetch_array($res);
				if(empty($data['team_tag']))
				{
					$query = "SELECT * FROM teams WHERE team_alias = '".$this->teamTag."'";
					$res = mysql_query($query);
					
					if($res == false)
						die(mysql_errno() . ": " . mysql_error() . "<br>");
					else
						$data = mysql_fetch_array($res);
				}
			}

			if(empty($data['team_tag']) && empty($data['team_alias']))
			{
				error_log("Can't find team with ".$this->teamTag.' team tag or alias in DB', 0);
				$this->teamLogoSrc = $this->PATH_TO_SAVE."no logo.png";
			}
			else
			{
				$this->teamTag = $data['team_tag'];
			
				for($i = 1; $i <= 5; $i++)
					$this->players[$i] = $data['player'.$i];


				$this->teamLogoSrc = $data['team_logo_src'];

				

				//echo '<br>team with tag/alias '.$this->teamTag.' was founded';
			}
			
	
		}


		/*
		*	Now this function usseles, becouse we have class team_info/classes/gosuTeamParser.php
		*	with better functionality
		*/

		public function getTeamProfileUrl($teamTag)
		{
			$teamTag = trim($teamTag);

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			// *** $url - url of team list page

			$teamTag = str_replace(" ", "+", $teamTag);

			$url = 'http://www.gosugamers.net/counterstrike/teams/?name='.$teamTag;

			$html = file_get_html($url);
			$html = $html->find("div.details a");

			// *** If search succesful

			if($html != null)
			{
				$teamUrl = 'http://www.gosugamers.net'.$html[0]->href;
				//echo $html[0]->plaintext.' and his url: '.$teamUrl.'<br>';

				// *** $teamProfile[team tag] = team url

				$teamProfile[$html[0]->plaintext] = $teamUrl;
				return $teamProfile;
			}
			else{
				die("Can't find team with tag ".$teamTag);
			}
		}

	}

	//$team = new Team();
	//$team->getTeamProfileUrl('Virtus.pro');
	//$team->uploadFileFromUrl("http://www.gosugamers.net/uploads/images/teams/7431-1403712011.jpeg");
?>