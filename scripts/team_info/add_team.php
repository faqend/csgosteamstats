<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/scripts/team_info/classes/add_team_lib.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/scripts/team_info/classes/gosuTeamParser.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/scripts/core/classes/fileUploader.php");
	//$player[1] = "Ya"; $player[2] = "Ti"; $player[3] = "Mi"; $player[4] = "Oni"; $player[5] = "KTO";
	
	set_time_limit(0);

	// *** Work with gosu parser:
	
	$gosuTeamParser = new GosuTeamParser();
	$gosuTeamParser->parseTeamName();
	$gosuTeamParser->parseTeamUrl();
	$gosuTeamParser->parseTeamLogoUrl();

	// *** Now $gosuTeamParser contains teams tag, alias, profile url and logo url.
	
	foreach ($gosuTeamParser->teamNameList as $teamTag => $teamAlias) {
		$fileUploader = new FileUploader($teamTag);
		$fileUploader->uploadFileFromUrl($gosuTeamParser->teamLogoUrlList[$teamTag]);
		$team = new Team($teamTag, $teamAlias, null, $fileUploader->linkToUploadedFile);
		$team->exportToDB();
	}
	
	//$fileUploader = new FileUploader("Team Tosha.cs");
	//$fileUploader->uploadFileFromUrl("http://www.gosugamers.net/uploads/images/teams/no-image.png");

	// DEBUG
	
	//$team = new Team("Test in Pyjamas", "TiP", null, "http://csteamstats.lc/img/team_logo/dignitas.png");
	//$team->exportToDB();


	//header('Refresh: 1; URL=http://csteamstats.lc/team/');
	//echo 'Команда успешно добавлена, через 1 сек. вы будете перенаправлены обратно.';
?>