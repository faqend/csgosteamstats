<?php
	class MatchPrinter
	{

		public function printMatch()
		{
			include_once('/match_parser.php');
			include_once($_SERVER['DOCUMENT_ROOT'].'/scripts/team_info/classes/add_team_lib.php');

			// *** Create new class instance. Games Pareser - class for parse games from csgolounge.com

			$matchParser = new MatchParser();
			$matchParser->parseMatchesList();

			// *** Create new class instance.


			foreach($matchParser->matchesList as $match)
			{
				$teamTag1 = substr($match, 0, strpos($match, ' '));
				$teamTag2 = substr($match, strrpos($match, ' '), strlen($match) - strrpos($match, ' '));

				$teamTag1 = trim($teamTag1);
				$teamTag2 = trim($teamTag2);

				$team1 = new Team($teamTag1, "", null, "");
				$team2 = new Team($teamTag2, "", null, "");

				/*
				*	DEBUG: look for chars in string
				*/
				/*
				foreach (count_chars($team1->teamTag, 1) as $i => $val) {
					echo "\"" , chr($i) , "\" встречается в строке $val раз(а).\n";
				}
				*/

				$team1->importFromDB();
				$team1_logo = str_replace(" ", "%20", $team1->teamLogoSrc);

				$team2->importFromDB();
				$team2_logo = str_replace(" ", "%20", $team2->teamLogoSrc);

				//echo 'this is team1 = '.$team1. ' and this is team2 = '.$team2.'<br>';
				printf('<div class="block_match">
							<div class="team_text">
								<div class="team_logo_small" style = "background: url(%s); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>
								<div class="team_tag">%s</div>
							</div>
						', $team1_logo, $team1->teamTag);
						echo'<div style = "float:left; width:10%; text-align:center; padding: 5% 0px">vs</div>';
					printf('<div class="team_text">
								<div class="team_logo_small" style = "float:right; background: url(%s); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>
								<div class="team_tag">%s</div>
							</div>
						</div>
							', $team2_logo, $team2->teamTag);
				unset($team1);
				unset($team2);
			}
			
		}

		
	}

?>