<?php
	class MatchParser
	{

		public $matchesList = array();

		public function parseMatchesList()
		{
			$url = "http://csgolounge.com/";

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			$html = file_get_html($url);

			$html = $html->find("div.match");

			$i = 0;

			foreach($html as $div)
			{
				//echo $div->plaintext."<br>";
				$teamTag1 = $div->plaintext;
				$teamTag1 = substr($teamTag1, 0, strpos($teamTag1, "\r"));
				$teamTag2 = $div->plaintext;
				$buff = substr($teamTag2, strrpos($teamTag2, "\r"), strlen($teamTag2) - strpos($teamTag2, "\r"));
				$teamTag2 = str_replace($buff, "", $teamTag2);
				$teamTag2 = substr($teamTag2, strrpos($teamTag2, " "), strlen($teamTag2) - strrpos($teamTag2, " "));
				//echo $teamTag1.' VS '.$teamTag2.'<br>';
				$this->matchesList["match".$i] = $teamTag1.' VS '.$teamTag2;
				$this->matchesList["match".$i] = trim($this->matchesList["match".$i]);
				//echo $this->matchesList["match".$i].'<br>';
				$i++;
			}
		}

		public function parseMatchesResult()
		{
			$url = "http://csgolounge.com/";

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			$html = file_get_html($url);
			
			$html = $html->find("div.matchmain");

			/*
			*  	What is children() ?, this is elements of DOM Tree. 
			*	Read this for understand: http://simplehtmldom.sourceforge.net/manual.htm#section_traverse
			*/

			// *** Take matches that already ends and say who win in that matches
			foreach ($html as $div) 
			{
				// *** if(img "win" exist) - so this team win game
				// ** team 1

				if($div->children(1)->children(0)->children(0)->children(0)->children(0)->children(0) != null)
				{
					$teamTag = $div->children(1)->children(0)->children(0)->children(0)->children(1)->plaintext;
					//$teamTag = explode("\r", $teamTag);
					$teamTag = substr($teamTag, 0, strpos($teamTag, "\r"));
					$teamTag = trim($teamTag);
					echo "Team : ".$teamTag." win<br>";
				}

				// *** if(img "win" exist) - so this team win game
				// ** team 2

				if($div->children(1)->children(0)->children(0)->children(2)->children(0)->children(0) != null)
				{
					$teamTag = $div->children(1)->children(0)->children(0)->children(2)->children(1)->plaintext;
					$teamTag = substr($teamTag, 0, strpos($teamTag, "\r"));
					$teamTag = trim($teamTag);
					echo "Team : ".$teamTag." win<br>";
				}
			}

		}
	}

	//$matchParser = new MatchParser();
	//$matchParser->parseMatchesList();
?>