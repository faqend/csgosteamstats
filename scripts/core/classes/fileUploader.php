<?php
	class FileUploader
	{
		private $PATH_TO_SAVE = "../../img/team_logo/";
		private $teamTag;
		private $SITE_URL = "http://csteamstats.lc/img/team_logo/";
		public $linkToUploadedFile;

		function __construct($teamTag)
		{
			$teamTag = trim($teamTag);
			$this->teamTag = $teamTag;
		}

		/*
		*	Upload img from url to server
		*/

		public function uploadFileFromUrl($url)
		{
			if($url == 'http://www.gosugamers.net/uploads/images/teams/no-image.png')
			{
				$this->linkToUploadedFile = $this->SITE_URL.'no logo.png';
				echo 'url:'.$url.' and link: '.$this->linkToUploadedFile. '<br>';
				return;
			}
			else
			{
				if($file = file_get_contents($url))
				{
					//echo $url;
					file_put_contents($this->PATH_TO_SAVE.$this->teamTag.'.png', $file);
					$this->linkToUploadedFile = $this->SITE_URL.$this->teamTag.'.png';
					echo 'url:'.$url.' and link: '.$this->linkToUploadedFile. '<br>';
				}
				else
					die("Error in uploading of file from url");
			}
		}

		/*
		*	This function upload team logo on server
		*	
		*	ToDo: Maybe need to add some better protect
		*/

		public function uploadFileFromPC()
		{
			if(is_uploaded_file($_FILES['team_logo']['tmp_name']))
			{
				move_uploaded_file($_FILES['team_logo']['tmp_name'], $this->PATH_TO_SAVE.$this->teamTag.".png");
				//echo("File was saved succesful <br>");
			}else {
				exit("Error of file loading");
			}
		}
	}

	//$fileUploader = new FileUploader("Bad Aim.cs");
	//$fileUploader->uploadFileFromUrl("http://www.gosugamers.net/uploads/images/teams/10962-1425422104.jpeg");
?>