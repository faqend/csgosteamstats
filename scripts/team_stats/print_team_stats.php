<?php
	include_once("/classes/team_stats_lib.php");

	if(!empty($_POST['team_tag']))
	{
		$teamStats = new TeamStats($_POST['team_tag']);
		$result = $teamStats->getTeamStats();
		if($result == true)
		{
			$teamStats->calculateTeamRank();
			echo json_encode($teamStats);
		}
		else
			echo json_encode("error");
	}
	/*
	// *** Create and calculate first team
	$teamStatsFirst = new TeamStats('fnatic');
	$teamStatsFirst->getTeamStats();
	$teamStatsFirst->calculateTeamRank();

	// *** Create and calculate second team
	$teamStatsSecond = new TeamStats('NiP');
	$teamStatsSecond->getTeamStats();
	$teamStatsSecond->calculateTeamRank();

	// *** Calculate percents
	$percent = ($teamStatsFirst->teamRank + $teamStatsSecond->teamRank) / 100;
	$percentFirst = round($teamStatsFirst->teamRank / $percent);
	$percentSecond = round($teamStatsSecond->teamRank / $percent);

	echo json_encode("Team ".$teamStatsFirst->teamStats['team_tag']. " have ". $percentFirst."% and ".$percentSecond."% on Team ".$teamStatsSecond->teamStats['team_tag']);
	*/
?>