$('select').change( function() {
	var teamTag = this.value;
	if(teamTag != "No team selected")
	{
		$.ajax({
			type: "POST",
			url: "../scripts/team_stats/print_team_stats.php",
			data: { team_tag: teamTag }
		})
		.done(function(data) {
			var teamStats = JSON.parse(data);
			if(teamStats == "error")
			{
				$('ul[class = "team_stats"]').css("visibility", "hidden");
				$('div#block_team_stats').css("visibility", "hidden");
			}
			else
			{
				$('ul[class = "team_stats"]').css("visibility", "visible");
				$('div#block_team_stats').css("visibility", "visible");
				$('li[id = "stats_maps_played"]').text("Maps played: "+teamStats['teamStats']['Maps_played']);
				$('li[id = "stats_wins"]').text("Wins: "+teamStats['teamStats']['Wins']);
				$('li[id = "stats_loses"]').text("Loses: "+teamStats['teamStats']['Loses']);
				$('li[id = "stats_kd_ratio"]').text("Kill/Death ratio: "+teamStats['teamStats']['KD_ratio']);
				$('li[id = "stats_rank"]').text("Total skill score: "+teamStats['teamRank']);
			}
		})
	}
	else
	{
		$('ul[class = "team_stats"]').css("visibility", "hidden");
		$('div#block_team_stats').css("visibility", "hidden");
	}
});