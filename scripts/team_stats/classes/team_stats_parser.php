<?php
	class TeamStatsParser
	{
		public $stats = array();// *** After function parseTeamStats() this array will contain team stats
		public $teamListHltvUrl = array();// *** After fucntion parseTeamUrl() this array will contain teams url

		// *** Function take url wit team stats and create array with team stats

		public function parseTeamStats($url)
		{

			// *** Include lib for easy parse sites

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			// *** MUST HAVE! Error without this string

			$url = htmlspecialchars_decode($url);

			// *** Easy way to parse team stats, if u have url link on stats of this team on hltv.org

			$html = file_get_html($url);
			$html = $html->find("div.covSmallHeadline");

			$isStats = false;
			$isIndex = true;
			$index = '';
			$data = '';

			foreach($html as $div)
			{
				if($isStats == true)
				{
					if($isIndex == true)
					{
						$text = str_replace("/", "", $div->plaintext);
						$text = str_replace(" ", "_", $text);
						$text = str_replace("__", "_", $text);
						//echo 'This is index - '.$text."<br>";
						$index = $text;
						$isIndex = false;
					}
					else
					{
						$text = $div->plaintext;
						//echo 'This is data - '.$text."<br>";
						$data = $text;
						$isIndex = true;
					}

					if($index != "" && $data != "")
					{
						if($index == "Best_player_(Average_rating)")
							$index = 'Best_player';
						if($index == "KD_Ratio")
							$index = 'KD_ratio';
						if($index == "Wins_draws_losses")
						{
							$buffer = substr($data, 0, strpos($data, '/')-1);
							//echo $buffer.'<br>';
							$this->stats['Wins'] = $buffer;
							$buffer = substr($data , strpos($data, '/')+2, strrpos($data, '/') - strpos($data, '/')-3);
							//echo $buffer.'<br>';
							$this->stats['Draws'] = $buffer;
							$buffer = substr($data, strrpos($data, '/')+2, strlen($data)-strrpos($data, '/')-2);
							//echo $buffer.'<br>';
							$this->stats['Loses'] = $buffer;
						}else {
							$this->stats[$index] = $data;
						}

						$index = "";
						$data = "";

					}
				}
				if($div->plaintext == 'Key stats')
					$isStats = true;
			}

			unset($html);
		}

		// *** Function parse page with teams and take url on teams pages

		public function parseTeamUrl()
		{
			$url = 'http://www.hltv.org/?pageid=182&eventid=0&gameid=2';

			require_once($_SERVER['DOCUMENT_ROOT']."/scripts/external_libs/simple_html_dom.php");

			$html = file_get_html($url);
			$html = $html->find("div.covSmallHeadline a");

			$isStarted = false;

			foreach($html as $div)
			{

				// *** MAGIC DO NOT TUCH THIS!!!
				
				if($isStarted == false)
				{
					$isStarted = true;
					continue;
				}

				//echo $div->plaintext.' and his url '.$div->href.'<br>';
				$teamTag = $div->plaintext;
				//echo '<br>TEAM URL HERE: '.$teamTag;
				$teamUrl = 'http://www.hltv.org/'.$div->href;
				
				if(array_key_exists($teamTag, $this->teamListHltvUrl) == false)
					$this->teamListHltvUrl[$teamTag] = $teamUrl;
			}

			unset($html);
		}

		// *** Function print teams stats on page. Need for easy DEBUG

		public function printTeamStats()
		{
			
			$this->parseTeamUrl();

			foreach($this->teamListHltvUrl as $key => $url)
			{
				$this->parseTeamStats($url);
				echo $key;
				echo '<br><br>'.var_dump($this->stats);
			}
		}

		// *** Function export to Data Base team stats from array $this->stats

		public function exportToDB()
		{
			include_once($_SERVER['DOCUMENT_ROOT']."/scripts/db/config.php");

			$this->parseTeamUrl();

			foreach($this->teamListHltvUrl as $teamTag => $url)
			{
				$this->parseTeamStats($url);

				// *** IF TEAM EXIST -> UPDATE || ELSE || IF TEAM NOT EXIST -> INSERT INTO

				$query1 = "SELECT * FROM team_stats WHERE team_tag = '".$teamTag."'";

				if($result1 = mysql_query($query1)){
					
					$data = mysql_fetch_array($result1);
					if($teamTag == $data['team_tag'])
					{
						// *** Team exist and will be updated in DataBase here
						
						$query2 = 'UPDATE team_stats SET id="'.$data['id'].'", team_tag="'.$teamTag.'", Maps_played="'.$this->stats['Maps_played'].'", Wins="'.$this->stats['Wins'].'", Draws="'.$this->stats['Draws'].'", Loses="'.$this->stats['Loses'].'", Total_kills="'.$this->stats['Total_kills'].'", Total_deaths="'.$this->stats['Total_deaths'].'", Rounds_played="'.$this->stats['Rounds_played'].'", KD_ratio="'.$this->stats['KD_ratio'].'", Best_player="'.$this->stats['Best_player'].'" WHERE team_tag="'.$teamTag.'"';
						
						if($result2 = mysql_query($query2)){
							echo 'Team: '.$teamTag. ' was succesful UPDATED in DataBase<br>';
						}else {
							die(mysql_errno() . ": " . mysql_error() . "<br>");
						}
					}
					else 
					{
						// *** Team dosn't exist and will be inserted in DataBase here

						$query3 = 'INSERT INTO `csteamstats`.`team_stats`(id, team_tag, Maps_played, Wins, Draws, Loses, Total_kills, Total_deaths, Rounds_played, KD_ratio, Best_player) VALUES("NULL", "'.$teamTag.'", "'.$this->stats['Maps_played'].'", "'.$this->stats['Wins'].'", "'.$this->stats['Draws'].'", "'.$this->stats['Loses'].'", "'.$this->stats['Total_kills'].'", "'.$this->stats['Total_deaths'].'", "'.$this->stats['Rounds_played'].'", "'.$this->stats['KD_ratio'].'", "'.$this->stats['Best_player'].'")';
						
						if($result3 = mysql_query($query3)){
							echo 'Team: '.$teamTag. ' was succesful INSERTED in DataBase<br>';
						}else {
							die(mysql_errno() . ": " . mysql_error() . "<br>");
						}
					}
					
				}else {
					die(mysql_errno() . ": " . mysql_error() . "<br>");
				}
			}
		}


	}


	// ** DEBUG **
	$teamStats = new TeamStatsParser();
	$teamStats->exportToDB();

?>