<?php
	class TeamStats
	{
		public $teamStats;
		public $teamTag;
		public $teamRank;

		function __construct($teamTag)
		{
			$teamTag = trim($teamTag);
			$this->teamTag = $teamTag;
		}

		public function getTeamStats()
		{
			if($this->teamTag != "")
			{
				$teamData = $this->importFromDB();
				if(!empty($teamData))
				{
					$teamData = mysql_fetch_array($teamData);
					if(!empty($teamData['team_tag']))
					{
						foreach($teamData as $index => $data)
						{
							if(is_string($index))
							{
								$this->teamStats[$index] = $data;
								//echo '<br>'.$index.' : '. $data;
							}
						}
						return true;
					}
					else
					{
						error_log("Team data was empty");
						//die("Team data was empty");
						return false;
					}
					
				}else{
					die("Team data was empty");
				}
			}else {
				die("No team tag here");
			}

		}


		// *** Import team stats from DataBase by team tag

		public function importFromDB()
		{
			include_once($_SERVER['DOCUMENT_ROOT']."/scripts/db/config.php");

			$query = 'SELECT * FROM team_stats WHERE team_tag="'.$this->teamTag.'"';
			
			if($result = mysql_query($query))
			{
				return $result;
			}else {
				die(mysql_errno() . ": " . mysql_error() . "<br>");
			}
		}

		public function calculateTeamRank()
		{
			// *** Get team rank
			$this->teamRank = $this->teamStats['Team_rank'];
			
			// *** For every 50 "Maps_played" team receive 5 points
			$this->teamRank += (integer)(($this->teamStats['Maps_played'] / 50) * 5);
			
			// *** For every 10 "Wins" team receive 1 point
			//$this->teamRank += (integer)($this->teamStats['Wins'] / 10);
			// *** For every 10 "Loses" team lose 0.5 point
			//$this->teamRank -= (integer)(($this->teamStats['Loses'] / 10) * 0.5);
			
			// *** "WINS/LOSES ratio" * 10
			$this->teamRank += (integer)(($this->teamStats['Wins'] / $this->teamStats['Loses']) * 10);
			
			// *** If "KD_ratio" > 1.0 then team receive 10 points, else 5 points
			if($this->teamStats['KD_ratio'] > 1.0)
				$this->teamRank += 10;
			else
				$this->teamRank += 5;

			//echo "Current team rank of ".$this->teamStats['team_tag']." team = ".$this->teamRank.'<br>';
		}

	}
?>