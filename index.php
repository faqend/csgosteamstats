<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title>Home page</title>
		<link rel = "stylesheet" href = "/css/style.css" type = "text/css">
	</head>
	<body>
		<header>
			<ul class = "menu">
				<li><a href = "/">Home</a></li>
				<li><a href = "/team/">Team stats</a></li>
				<li><a href = "/matches/">Matches</a></li>
			</ul>
		</header>
		<div id = "wrapper">
			<div id = "main_block">

			</div>
		</div>
	</body>
</html>